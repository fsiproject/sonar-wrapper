# Sonar Wrapper

Small sonar-scanner wrapper script to detect perl, bash or python code for files without extension  
The detection is handled by the **linux file** command.   
Sonar-wrapper will check and install the file command via **apk**.  

### Example CI/CD .gitlab-ci.yml config file

```
stages:
 - sonarqube

    
include:
  - remote: 'https://gitlab.com/fsiproject/sonar-wrapper/-/raw/main/sonar-gitlab-ci.yml'

